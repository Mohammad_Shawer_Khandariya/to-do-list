window.addEventListener('DOMContentLoaded', updateToDo )

function updateToDo() {
    console.log("I am working")
    // localStorage.clear()
    for (let key of Object.keys(localStorage)) {
        let data = localStorage.getItem(key)
        console.log(data)
        addList(data, key)
    }
}

function checkEvent(event) {
    const type = event.target.type
    if(type === "checkbox") {
        isCompleted(event.target);
    } else if (type === "submit") {
        deleteTask(event.target)
    } else if (type === "reset") {
        editTask(event.target)
    }
}

function addTask(value) {
    let taskValue;
    if (value) {
        taskValue = value
    } else {
        let input = document.getElementById('add-task')
        taskValue = input.value
        input.value = "";
    }
    // console.log(taskValue)
    if (!taskValue) {
        alert("Invalid Input!")
    } else {
        let id = createUniqueId(taskValue)
        let data = JSON.stringify([taskValue, false])
        setKeyValue(id, data)
        addList(data, id)
    }
    return false;
}

function addList(data, key) {
    let lists = document.getElementById("mylist")
    let list = createList(data, key);
    lists.appendChild(list);
    let checkbox = createCheckbox(key, data);
    let button = createDeleteButton();
    let edit = createEditButton();
    list.prepend(checkbox);
    list.appendChild(edit);
    list.appendChild(button);
}

function createList(data, key) {
    let list = document.createElement("li");
    list.innerText = JSON.parse(data)[0]
    list.id = key;
    return list;
}

function createUniqueId(data) {
    let randomNum = Math.floor((Math.random() * 899) + 100)
    if (data.length >= 3) {
        return(data.slice(0,3) + randomNum)
    } else {
        return data + randomNum
    }
}

function createCheckbox(key) {
    let checkbox = document.createElement("input")
    checkbox.type = "checkbox"
    let data = JSON.parse(localStorage.getItem(key));
    console.log(data)
    if (data[1]) {
        let list = document.getElementById(key)
        console.log(list);
        list.classList.add("complete");
        checkbox.checked = true;
    }
    return checkbox
}

function isCompleted(e) {
    const checkboxParent = e.parentElement
    const key = checkboxParent.id
    const arrValue = JSON.parse(localStorage.getItem(key))
    // console.log(typeof arrValue)
    // console.log(key)
    if (e.checked) {
        arrValue[1] = true;
        localStorage.setItem(key, JSON.stringify(arrValue))
        checkboxParent.classList.add("complete");
    } else {
        arrValue[1] = false;
        localStorage.setItem(key, JSON.stringify(arrValue))
        checkboxParent.classList.remove("complete")
    }
}

function createDeleteButton() {
    let button = document.createElement("button")
    button.innerText = "Delete"
    return button;
}

function deleteTask(e) {
    let deleteParent = document.getElementById(e.parentElement.id)
    console.log(deleteParent.id);
    localStorage.removeItem(deleteParent.id)
    deleteParent.remove()
}

function createEditButton() {
    let button = document.createElement("button")
    button.innerText = "Edit"
    button.type = "reset"
    return button
}

function editTask(e) {
    let editParent = document.getElementById(e.parentElement.id)
    let previousValue = e.previousSibling.textContent.trim()
    const newValue = window.prompt("Edit Task", previousValue)
    let key = editParent.id;
    if (!newValue) {
        e.previousSibling.textContent = previousValue;
    } else if (newValue !== previousValue) {
        e.previousSibling.textContent = newValue;
        checkbox = editParent.firstElementChild
        setKeyValue(key, JSON.stringify([newValue, checkbox.checked]))
    }
}

function setKeyValue(key, value) {
    localStorage.setItem(key, value)
}